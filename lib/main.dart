import 'package:first/src/pages/home.dart';
import 'package:first/src/pages/welcome_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _route = <String, WidgetBuilder>{
    "/page2": (BuildContext context) => WelcomePage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: _route,
      home: const Home(title: 'First Navigator'),
    );
  }
}


