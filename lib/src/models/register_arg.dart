class RegisterArg {
  final String reportEmail;
  final String reportPassword;
  final String reportPhone;

  RegisterArg(this.reportEmail, this.reportPassword,this.reportPhone);
}
