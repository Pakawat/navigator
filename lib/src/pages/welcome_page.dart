import 'package:flutter/material.dart';
import '../models/register_arg.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final RegisterArg args =
    ModalRoute.of(context)!.settings.arguments as RegisterArg;
    return Scaffold(appBar: AppBar(
      title: Text("Welcome ${args.reportEmail}"),
    ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Hello ${args.reportEmail}\n"),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Email: ${args.reportEmail}\n'),
                  Text('Password: ${args.reportPassword}\n'),
                  Text('Phone: ${args.reportPhone}\n'),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context, "Hello ${args.reportEmail}");
                },
                child: Text("Back"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
