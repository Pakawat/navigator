import 'package:flutter/material.dart';

import '../models/register_arg.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController emailController = TextEditingController();
  TextEditingController pwdController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: emailController,
                //obscureText: true,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.black,),
                  filled: true,
                  fillColor: Colors.blue[100],
                  border:
                  OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  //border:InputBorder.none,
                  hintText: 'PLEASE ENTER YOUR EMAIL',
                  hintStyle: TextStyle(color: Colors.black),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: pwdController,
                //obscureText: true,
                textAlign: TextAlign.left,
                obscureText: true,
                decoration: InputDecoration(
                  //border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.password,
                    color: Colors.black,),
                  filled: true,
                  fillColor: Colors.blue[100],
                  border:
                  OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  hintText: 'PLEASE ENTER YOUR PASSWORD',
                  hintStyle: TextStyle(color: Colors.black),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: phoneController,
                //obscureText: true,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.phone,
                    color: Colors.black,),
                  filled: true,
                  fillColor: Colors.blue[100],
                  border:
                  OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  //border:InputBorder.none,
                  hintText: 'PLEASE ENTER PHONE NUMBER',
                  hintStyle: TextStyle(color: Colors.black),
                ),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  // Clear button
                  child: const Text('Clear'),
                  onPressed: () {
                    emailController.clear();
                    pwdController.clear();
                    phoneController.clear();
                  },
                ),

                ElevatedButton(
                  onPressed: () async {
                    var result = await Navigator.pushNamed(context, "/page2",
                        arguments: RegisterArg(
                            emailController.text, pwdController.text, phoneController.text));
                    print(result);
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(content: Text("$result")));
                  },
                  child: Text("Next Page"),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                backgroundColor: Colors.blue[100],
                // Retrieve the text the user has entered by using the
                // TextEditingController.
                content: Column(
                  children: [
                    Icon(Icons.info),
                    Text("Your Info\n"),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Email: ${emailController.text}\n'),
                        Text('Password: ${pwdController.text}\n'),
                        Text('Phone: ${phoneController.text}\n'),
                      ],
                    ),
                  ],
                ),
              );
            },
          );
        },
        tooltip: 'Next Page',
        child: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}
